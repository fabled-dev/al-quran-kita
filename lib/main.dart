import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'apps/theme/app_theme_controller.dart';
import 'routes.dart';
import 'setup.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setup();
  runApp(QuranApp());
}

class QuranApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ThemeController>(
      init: ThemeController(),
      builder: (themeController) {
        return GetMaterialApp(
          title: 'Al Quran Kita',
          debugShowCheckedModeBanner: false,
          theme: themeController.appTheme.themeData,
          getPages: routes,
          initialRoute: '/',
        );
      },
    );
  }
}
