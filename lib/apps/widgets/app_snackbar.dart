import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppSnackBar {
  static simple(String text) {
    if (Get.isSnackbarOpen) Get.back();

    Get.snackbar(
      null,
      null,
      snackPosition: SnackPosition.BOTTOM,
      instantInit: true,
      maxWidth: 250.0,
      margin: EdgeInsets.fromLTRB(0, 0, 0, 24.0),
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      backgroundColor: Colors.black.withAlpha(150),
      messageText: Text(
        text,
        maxLines: 2,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 11.5, color: Colors.white, height: 1.5),
      ),
    );
  }
}
