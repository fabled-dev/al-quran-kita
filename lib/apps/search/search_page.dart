import 'search_controller.dart';
import 'search_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../home/home_controller.dart';

class SearchPage extends StatelessWidget {
  final homeController = HomeController.to;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SearchController>(
      init: SearchController(),
      builder: (searchController) {
        return WillPopScope(
          onWillPop: () {
            homeController.toogleModeSearch();
            return Future.value(false);
          },
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.white10,
              title: buildSearchText(),
              elevation: 0.0,
            ),
            body: buildListView(),
          ),
        );
      },
    );
  }

  Widget buildSearchText() {
    final searchController = SearchController.to;

    return Container(
      child: Stack(
        alignment: Alignment.centerRight,
        children: <Widget>[
          TextField(
            controller: searchController.searchText,
            focusNode: searchController.searchFocus,
            onEditingComplete: () => searchController.find(),
            autofocus: true,
            decoration: InputDecoration(
              hintText: "Minimal 3 Huruf",
              suffixIcon: Icon(Icons.close),
            ),
          ),
          IconButton(
            icon: Icon(Icons.close),
            iconSize: 24.0,
            color: Colors.grey[600],
            onPressed: () {
              homeController.toogleModeSearch();
            },
          ),
        ],
      ),
    );
  }

  Widget buildListView() {
    final searchController = SearchController.to;

    if (searchController.showRecent) return Container();

    return Container(
      margin: EdgeInsets.only(left: 12.0, top: 16.0, right: 16.0),
      child: ListView.builder(
        physics: BouncingScrollPhysics(),
        itemCount: searchController.suras.length,
        itemBuilder: (context, index) {
          final sura = searchController.suras[index];
          return SearchView(sura: sura, findText: searchController.searchText.text);
        },
      ),
    );
  }
}
