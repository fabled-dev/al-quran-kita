import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../sura/sura.dart';
import '../sura/sura_repo.dart';
import '../widgets/app_snackbar.dart';

class SearchSuraItem {
  final Sura sura;
  final bool isExpanded;

  SearchSuraItem({this.sura, this.isExpanded});
}

class SearchController extends GetxController {
  bool showRecent = true;
  String oldValue;
  TextEditingController searchText = TextEditingController();
  FocusNode searchFocus = FocusNode();
  // List<SearchSuraItem> suras = [];
  List<Sura> suras = [];

  static SearchController get to => Get.find();

  final suraRepo = SuraRepo.to;

  @override
  onInit() {
    final onChangeFocus = () {
      if (!searchFocus.hasFocus) {
        showRecent = true;
        return update();
      }
      showRecent = false;
      return update();
    };
    searchFocus.addListener(onChangeFocus);
  }

  void find() async {
    if (searchText.text.length < 3) return AppSnackBar.simple('Pencarian minimal 3 huruf');
    if (oldValue == searchText.text) {
      Get.focusScope.unfocus();
      showRecent = false;
      update();
      return;
    }

    Get.focusScope.unfocus();
    suras = await suraRepo.findWithVerses(word: searchText.text);
    showRecent = false;
    oldValue = searchText.text;
    update();
  }

  @override
  onClose() {
    searchFocus.dispose();
  }
}
