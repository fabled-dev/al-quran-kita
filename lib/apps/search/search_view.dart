import 'package:alquran_kita/apps/verse/verse_args.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../sura/sura.dart';
import '../theme/app_theme_controller.dart';
import '../verse/verse.dart';

class SearchView extends StatelessWidget {
  final Sura sura;
  final String findText;

  SearchView({Key key, this.sura, this.findText}) : super(key: key);

  final appTheme = ThemeController.to.appTheme;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            color: Colors.grey[200],
          ),
          child: Row(
            children: [
              Text(
                sura.latinName,
                style: TextStyle(color: appTheme.darkPrimaryColor, fontSize: 16.5, fontWeight: FontWeight.w600),
              ),
              SizedBox(width: 4.0),
              Text("(${sura.verses.length} ayat)")
            ],
          ),
        ),
        ListView.separated(
          itemCount: sura.verses.length,
          shrinkWrap: true,
          primary: false,
          separatorBuilder: (context, index) => Divider(indent: 8.0, endIndent: 4.0, color: Colors.grey[400]),
          itemBuilder: (context, index) {
            return buildVerse(sura.verses[index]);
          },
        ),
        SizedBox(height: 16.0),
      ],
    );
  }

  Widget buildVerse(Verse verse) {
    List<TextSpan> textSpans = [];
    String verseText = verse.nameTranslation;

    while (true) {
      int indexOf = verseText.indexOf(RegExp(findText, caseSensitive: false));
      if (indexOf == -1) {
        textSpans.add(TextSpan(text: verseText));
        break;
      }

      String startText = verseText.substring(0, indexOf);
      textSpans.add(TextSpan(text: startText));

      String find = verseText.substring(indexOf, indexOf + findText.length);
      textSpans.add(TextSpan(text: find, style: TextStyle(color: appTheme.primaryColor)));

      verseText = verseText.substring(indexOf + findText.length);
    }

    return GestureDetector(
      onTap: () {
        final args = VerseArgs(sura: sura, viewBy: ViewBy.sura, jumpTo: verse.aya);
        Get.toNamed("/verse", arguments: args);
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(8.0, 8.0, 4.0, 8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('${sura.number} : ${verse.aya}', style: TextStyle(fontSize: 16.0)),
            SizedBox(height: 4.0),
            RichText(
              text: TextSpan(
                style: TextStyle(color: Colors.black87, fontFamily: "Touche", fontSize: 14.5, height: 1.8),
                children: textSpans,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
