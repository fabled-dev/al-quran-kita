import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../sura/sura.dart';
import '../verse/verse.dart';
import 'last_read.dart';

enum AppTab { surah, juz, bookmark }

class HomeController extends GetxController {
  AppTab activeTab = AppTab.surah;
  bool isModeSearch = false;
  LastRead lastRead;

  static HomeController get to => Get.find();
  static init() async {
    Get.put(HomeController());
  }

  @override
  onInit() {
    getLastRead();
    super.onInit();
  }

  void getLastRead() {
    final box = GetStorage();
    Map<String, dynamic> jsonLastRead = box.read('lastRead');
    if (jsonLastRead.isNull) return;
    lastRead = LastRead.fromJson(jsonLastRead);
  }

  void saveLastRead(Sura sura, Verse verse) {
    final box = GetStorage();
    lastRead = LastRead(sura: sura, verse: verse);
    box.write('lastRead', lastRead.toJson());
    update(['lastRead']);
  }

  void removeLastRead() {
    final box = GetStorage();
    box.remove('lastRead');
    lastRead = null;
    update(['lastRead']);
  }

  void toogleModeSearch() {
    isModeSearch = !isModeSearch;
    update();
  }

  void changeTab(AppTab value) {
    activeTab = value;
    update(['tab']);
  }
}
