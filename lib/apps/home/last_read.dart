import '../sura/sura.dart';
import '../verse/verse.dart';

class LastRead {
  final Sura sura;
  final Verse verse;

  LastRead({this.sura, this.verse});

  factory LastRead.fromJson(json) {
    return LastRead(
      verse: Verse.fromJson(json['verse']),
      sura: Sura.fromJson(json['sura']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'verse': verse.toJson(),
      'sura': sura.toJson(),
    };
  }
}
