import 'package:alquran_kita/apps/theme/app_theme.dart';
import 'package:alquran_kita/apps/theme/app_theme_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../widgets/app_icon_icons.dart';
import '../verse/verse_args.dart';
import 'home_controller.dart';

class LastReadView extends StatelessWidget {
  final AppTheme appTheme = Get.find<ThemeController>().appTheme;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      id: 'lastRead',
      builder: (homeController) {
        final lastRead = homeController.lastRead;

        if (lastRead.isNull) return Container();

        return InkWell(
          onTap: () {
            final args = VerseArgs(
              sura: homeController.lastRead.sura,
              jumpTo: homeController.lastRead.verse.aya,
              viewBy: ViewBy.sura,
            );
            Get.toNamed("/verse", arguments: args);
          },
          child: Container(
            width: 180,
            constraints: BoxConstraints(maxWidth: 420),
            margin: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 12.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12.0),
              gradient: RadialGradient(
                center: Alignment(-1, -1),
                radius: 2.8,
                colors: [appTheme.gradientColorPrimary, appTheme.gradientColorSecondary],
                tileMode: TileMode.clamp,
              ),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12.0),
              child: Stack(
                children: [
                  Positioned(
                    right: -28.0,
                    bottom: -66.0,
                    child: Container(
                      height: 200,
                      width: 200,
                      // child: Opacity(opacity: 0.90, child: Image.asset('assets/quran.png')),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(AppIcon.book, size: 16.0, color: Colors.white),
                            SizedBox(width: 8.0),
                            Text("Terakhir dibaca", style: TextStyle(fontSize: 12, color: Colors.white, fontWeight: FontWeight.w500)),
                          ],
                        ),
                        SizedBox(height: 24),
                        Text(lastRead.sura.latinName, style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 16.0)),
                        SizedBox(height: 8),
                        Text("Ayat No: ${lastRead.verse.aya}", style: TextStyle(fontSize: 13.0, color: Colors.white.withAlpha(200))),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
