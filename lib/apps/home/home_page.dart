import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../bookmark/bookmark_page.dart';
import '../juz/juz_page.dart';
import '../search/search_page.dart';
import '../sura/sura_page.dart';
import '../theme/app_theme_controller.dart';
import '../widgets/app_icon_icons.dart';
import 'home_controller.dart';
import 'jump_verse_dialog.dart';

class HomePage extends StatelessWidget {
  final appTheme = ThemeController.to.appTheme;
  final homeController = HomeController.to;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (_) {
        if (homeController.isModeSearch) return SearchPage();

        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white10,
            title: Text("Al Quran Kita", style: appTheme.titleText),
            elevation: 0.0,
            actions: [
              IconButton(
                icon: Icon(Icons.low_priority),
                iconSize: 28.0,
                color: Colors.grey[700],
                onPressed: () => Get.dialog(JumpSuraDialog()),
              ),
              IconButton(
                icon: Icon(AppIcon.loupe),
                iconSize: 20.0,
                color: Colors.grey[700],
                onPressed: () => homeController.toogleModeSearch(),
              ),
            ],
          ),
          body: CupertinoScrollbar(
            child: GetBuilder<HomeController>(
              id: 'tab',
              builder: (_) {
                if (homeController.isModeSearch) return Container();

                if (homeController.activeTab == AppTab.surah) return SuraPage();
                if (homeController.activeTab == AppTab.juz) return JuzPage();
                if (homeController.activeTab == AppTab.bookmark) return BookmarkPage();

                return Container();
              },
            ),
          ),
        );
      },
    );
  }
}
