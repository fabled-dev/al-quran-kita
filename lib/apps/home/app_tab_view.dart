import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../theme/app_theme_controller.dart';
import 'home_controller.dart';

class AppTabView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(child: TabButton(appTab: AppTab.surah)),
          Expanded(child: TabButton(appTab: AppTab.juz)),
          Expanded(child: TabButton(appTab: AppTab.bookmark)),
        ],
      ),
    );
  }
}

class TabButton extends StatelessWidget {
  final AppTab appTab;
  final AppTheme appTheme = Get.find<ThemeController>().appTheme;

  TabButton({Key key, this.appTab}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (homeController) {
        final color = homeController.activeTab == appTab ? appTheme.primaryColor : Colors.grey;

        return InkWell(
          onTap: () => homeController.changeTab(appTab),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Text(describeEnum(appTab).capitalize, style: TextStyle(fontSize: 16.0, color: color, fontWeight: FontWeight.w600)),
              SizedBox(height: 16.0),
              Container(height: 2.0, color: color),
            ],
          ),
        );
      },
    );
  }
}
