import 'package:alquran_kita/apps/theme/app_theme_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../sura/sura.dart';
import '../sura/sura_controller.dart';
import '../verse/verse_args.dart';
import '../widgets/app_dropdown.dart' as custom;

class JumpSuraDialog extends StatefulWidget {
  final Sura sura;

  const JumpSuraDialog({Key key, this.sura}) : super(key: key);

  @override
  _JumpSuraDialogState createState() => _JumpSuraDialogState();
}

class _JumpSuraDialogState extends State<JumpSuraDialog> {
  List<Sura> suras = Get.find<SuraController>().suras;
  AppTheme appTheme = Get.find<ThemeController>().appTheme;

  Sura selectedSura;
  TextEditingController ayaController = TextEditingController();
  FocusNode ayaFocus = FocusNode();

  @override
  void initState() {
    super.initState();
    if (widget.sura == null) {
      selectedSura = suras[0];
      return;
    }

    selectedSura = suras.firstWhere((sura) => sura.number == widget.sura.number);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      elevation: 4.0,
      backgroundColor: Colors.white,
      contentPadding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 24.0),
      title: Row(
        children: [
          Icon(Icons.low_priority, size: 24.0),
          SizedBox(width: 16.0),
          Text("Pergi Ke Surah", style: TextStyle(fontSize: 16.0)),
        ],
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          custom.DropdownButton<Sura>(
            value: selectedSura,
            iconSize: 24,
            isExpanded: true,
            underline: Container(height: 1, color: Colors.grey[400]),
            onChanged: (Sura newValue) {
              setState(() {
                selectedSura = newValue;
                // ayaFocus.requestFocus();
              });
            },
            items: suras.map<custom.DropdownMenuItem<Sura>>((Sura sura) {
              return custom.DropdownMenuItem<Sura>(
                value: sura,
                child: Text(
                  "${sura.number}.\t ${sura.latinName}",
                  style: TextStyle(fontSize: 14.0),
                ),
              );
            }).toList(),
          ),
          SizedBox(height: 16.0),
          Text(
            "Masukkan nomor ayat antara 1 - ${selectedSura.versesCount}",
            style: TextStyle(fontSize: 12.0, color: Colors.grey[700]),
          ),
          Container(
            margin: EdgeInsets.only(top: 8.0),
            width: 52.0,
            child: TextFormField(
              keyboardType: TextInputType.number,
              textAlign: TextAlign.center,
              decoration: InputDecoration(isDense: true, hintText: "1 - ${selectedSura.versesCount}"),
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly,
                CustomRangeTextInputFormatter(selectedSura.versesCount),
              ],
              controller: ayaController,
              focusNode: ayaFocus,
              onFieldSubmitted: (value) => openVersePage(),
            ),
          ),
        ],
      ),
      actions: [
        FlatButton(
          splashColor: Colors.transparent,
          child: Text("BATAL", style: TextStyle(color: appTheme.primaryColor)),
          onPressed: () => Get.back(),
        ),
        FlatButton(
          splashColor: Colors.transparent,
          child: Text("OK", style: TextStyle(color: appTheme.primaryColor)),
          onPressed: openVersePage,
        ),
      ],
    );
  }

  openVersePage() {
    final jumpTo = ayaController.text.isEmpty ? 0 : int.tryParse(ayaController.text);
    final args = VerseArgs(jumpTo: jumpTo, sura: selectedSura, viewBy: ViewBy.sura);
    FocusScope.of(context).unfocus();
    final run = () => Get.offNamedUntil("/verse", (route) => route.settings.name == '/', arguments: args);
    if (widget.sura.isNull) return run();
    if (Get.currentRoute != '/verse' || selectedSura.number != widget.sura.number) return run();
    return Get.back(result: jumpTo);
  }
}

class CustomRangeTextInputFormatter extends TextInputFormatter {
  final int max;

  CustomRangeTextInputFormatter(this.max);

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text == '') return TextEditingValue();
    if (int.parse(newValue.text) < 1) return oldValue;
    return int.parse(newValue.text) > max ? oldValue : newValue;
  }
}
