import 'dart:math';

import 'package:firebase_admob/firebase_admob.dart';
import 'package:get/get.dart';

class AdmobController extends GetxController {
  static const rate = 30;
  static const String appId = "ca-app-pub-8484938858698814~3926014013";
  static const String bannerUnitId = " ca-app-pub-8484938858698814/2285629286";
  static const String interstitialUnitId = "ca-app-pub-8484938858698814/5879908295";
  static const String nativeUnitId = "ca-app-pub-8484938858698814/7973070238";

  static AdmobController get to => Get.find();
  static init() async {
    FirebaseAdMob.instance.initialize(appId: appId);
    Get.put(AdmobController());
  }

  Future<void> showInterstitialAd() async {
    int rng = new Random().nextInt(100);
    if (rng <= rate) return;

    InterstitialAd interstitialAd = InterstitialAd(
      adUnitId: interstitialUnitId,
      // adUnitId: InterstitialAd.testAdUnitId,
    );

    interstitialAd
      ..load()
      ..show();
  }
}
