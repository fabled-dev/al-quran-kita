import 'package:flutter/material.dart';

class AppTheme {
  final Color primaryColor;
  final Color darkPrimaryColor;
  final Color activeColor;
  final Color gradientColorPrimary;
  final Color gradientColorSecondary;
  TextStyle titleText;
  ThemeData themeData;

  AppTheme({
    this.primaryColor,
    this.darkPrimaryColor,
    this.activeColor,
    this.gradientColorPrimary,
    this.gradientColorSecondary,
  });

  AppTheme setup() {
    titleText = TextStyle(color: primaryColor, fontWeight: FontWeight.w700, fontSize: 18.0);

    themeData = ThemeData(
      fontFamily: "Touche",
      brightness: Brightness.light,
      primaryColor: primaryColor,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
    );

    return this;
  }
}
