import 'package:alquran_kita/apps/theme/app_theme.dart';
import 'package:alquran_kita/apps/theme/aquamanire_theme.dart';
import 'package:get/get.dart';

import 'purple_theme.dart';
export 'app_theme.dart';

class ThemeController extends GetxController {
  AppTheme appTheme;

  static ThemeController get to => Get.find();
  static init() async {
    Get.put(ThemeController());
  }

  @override
  onInit() {
    appTheme = aquamarineTheme.setup();
    super.onInit();
  }

  void change() {
    appTheme = violetTheme.setup();
    update();
  }
}
