import 'package:alquran_kita/apps/theme/app_theme.dart';
import 'package:flutter/material.dart';

final aquamarineTheme = AppTheme(
  primaryColor: const Color(0xff22D3C1),
  darkPrimaryColor: const Color(0xff063722),
  gradientColorPrimary: const Color(0xff40e0d0),
  gradientColorSecondary: const Color(0XffA0F8BF),
  activeColor: const Color(0xff1FC1B1),
);
