import 'package:alquran_kita/apps/theme/app_theme.dart';
import 'package:flutter/material.dart';

final violetTheme = AppTheme(
  primaryColor: Colors.purple[800],
  darkPrimaryColor: const Color(0xff230f4e),
  gradientColorPrimary: const Color(0xffdb94fa),
  gradientColorSecondary: const Color(0XFF9b5fff),
  activeColor: const Color(0xff873ed5),
);
