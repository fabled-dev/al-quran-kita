import 'package:alquran_kita/apps/sura/sura.dart';
import 'package:alquran_kita/apps/sura/sura_controller.dart';
import 'package:alquran_kita/apps/theme/app_theme_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ConfirmDownloadAudioDialog extends StatelessWidget {
  final AppTheme appTheme = Get.find<ThemeController>().appTheme;
  final Sura sura;

  ConfirmDownloadAudioDialog({Key key, this.sura}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Text(
        "Audio QS. ${sura.latinName} belum di download. Apakah ingin download terlebih dahulu ?",
        style: TextStyle(fontSize: 14, height: 1.6),
      ),
      actions: [
        FlatButton(
          splashColor: Colors.transparent,
          child: Text("BATAL", style: TextStyle(color: appTheme.primaryColor)),
          onPressed: () => Get.back(),
        ),
        FlatButton(
          splashColor: Colors.transparent,
          child: Text("OK", style: TextStyle(color: appTheme.primaryColor)),
          onPressed: () {
            Get.back();
            Get.dialog(DownloadAudioDialog(sura: sura), barrierDismissible: false);
            SuraController.to.downloadAllAudio(sura);
          },
        ),
      ],
    );
  }
}

class ExitDownloadAudioDialog extends StatelessWidget {
  final AppTheme appTheme = Get.find<ThemeController>().appTheme;
  final Sura sura;

  ExitDownloadAudioDialog({Key key, this.sura}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding: EdgeInsets.all(60.0),
      content: Text(
        "Apakah anda ingin membatalkan download ?",
        style: TextStyle(fontSize: 14, height: 1.6),
      ),
      actions: [
        FlatButton(
          splashColor: Colors.transparent,
          child: Text("TIDAK", style: TextStyle(color: appTheme.primaryColor)),
          onPressed: () => Get.back(),
        ),
        FlatButton(
          splashColor: Colors.transparent,
          child: Text("YA", style: TextStyle(color: Colors.red)),
          onPressed: () {
            SuraController.to.cancelDownload(sura: sura);
            Get.back();
            Get.back();
          },
        ),
      ],
    );
  }
}

class DownloadAudioDialog extends StatelessWidget {
  final AppTheme appTheme = Get.find<ThemeController>().appTheme;
  final Sura sura;

  DownloadAudioDialog({Key key, this.sura}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Get.dialog(ExitDownloadAudioDialog(sura: sura));
        return Future.value(true);
      },
      child: AlertDialog(
        title: Row(
          children: [
            Icon(Icons.file_download),
            SizedBox(width: 16.0),
            Text("Download"),
          ],
        ),
        content: Container(
          width: double.maxFinite,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Download Audio QS. ${sura.latinName}.", style: TextStyle(fontSize: 14, height: 1.6)),
              GetBuilder<SuraController>(
                id: 'totalAudio',
                builder: (_) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("${sura.totalAudio} / ${sura.versesCount}", style: TextStyle(fontSize: 14, height: 1.6)),
                      SizedBox(height: 32.0),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(16),
                        child: Container(
                          height: 8,
                          child: LinearProgressIndicator(
                            backgroundColor: Colors.grey[400],
                            valueColor: AlwaysStoppedAnimation<Color>(appTheme.primaryColor),
                            value: sura.percentDownloaded / 100,
                          ),
                        ),
                      ),
                      SizedBox(height: 8.0),
                      Text("${sura.percentDownloaded.toStringAsFixed(0)}%", style: TextStyle(fontSize: 14, color: Colors.grey[700])),
                    ],
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
