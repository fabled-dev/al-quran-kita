import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'audio_controller.dart';

class AudioPlayerView extends StatelessWidget {
  AudioPlayerView({Key key}) : super(key: key);
  final AudioController audioController = AudioController.to;
  final Color color = Colors.grey[800];

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AudioController>(
      id: "audioPlayer",
      builder: (audioController) {
        AudioPlayerState state = audioController.audioPlayer.state;
        if (state == null || state == AudioPlayerState.STOPPED || state == AudioPlayerState.COMPLETED) {
          return Container();
        }

        return Card(
          elevation: 4,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: Container(
            height: 40.0,
            width: 220.0,
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                IconButton(icon: Icon(Icons.skip_previous), color: color, onPressed: audioController.previous),
                _buildPlayButton(),
                IconButton(icon: Icon(Icons.stop), color: Colors.red, onPressed: audioController.stop),
                IconButton(icon: Icon(Icons.skip_next), color: color, onPressed: audioController.next),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildPlayButton() {
    AudioPlayerState state = audioController.audioPlayer.state;
    if (state == AudioPlayerState.PLAYING) {
      return IconButton(icon: Icon(Icons.pause), color: color, onPressed: audioController.pause);
    }

    return IconButton(icon: Icon(Icons.play_arrow), color: color, onPressed: audioController.resume);
  }
}
