import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../core/app_exception.dart';
import '../verse/verse.dart';
import '../verse/verse_controller.dart';

class AudioController extends GetxController {
  Directory dir;
  AudioPlayer audioPlayer = AudioPlayer();

  static AudioController get to => Get.find();
  static init() async {
    Get.put(AudioController());
  }

  @override
  void onInit() async {
    super.onInit();
    dir = await getApplicationDocumentsDirectory();
    audioPlayer.onPlayerCompletion.listen((event) => next());
    if (!Platform.isIOS) return;
    audioPlayer.startHeadlessService();
  }

  List<Verse> verses = [];
  int index = 0;
  void addPlayList(List<Verse> values) {
    verses = values;
  }

  Future<void> startPlay({Verse verse}) async {
    index = verses.indexOf(verse);
    String reciteBy = 'afasy';
    String path = "${dir.path}/audio/$reciteBy/${verse.audioPath}";
    bool isExist = await File(path).exists();
    if (!isExist) throw FileNotFoundException();

    if (audioPlayer.state == AudioPlayerState.PLAYING) await audioPlayer.stop();
    await audioPlayer.play(path, isLocal: true);
    update(['audioPlayer']);
  }

  Future<void> playFileAudio() async {
    String reciteBy = 'afasy';
    Verse verse = verses[index];
    String path = "${dir.path}/audio/$reciteBy/${verse.audioPath}";
    bool isExist = await File(path).exists();
    if (!isExist) audioPlayer.stop();
    VerseController.to.itemScrollController.scrollTo(index: index + 1, duration: Duration(milliseconds: 500));
    audioPlayer.play(path, isLocal: true);
    update(['audioPlayer']);
  }

  Future<void> next() async {
    index += 1;
    if (index < verses.length) return playFileAudio();
    await audioPlayer.stop();
    update(['audioPlayer']);
  }

  Future<void> previous() async {
    index -= 1;
    if (index > -1) return playFileAudio();
    await audioPlayer.stop();
    update(['audioPlayer']);
  }

  Future<void> pause() async {
    await audioPlayer.pause();
    update(['audioPlayer']);
  }

  Future<void> resume() async {
    await audioPlayer.resume();
    update(['audioPlayer']);
  }

  Future<void> stop() async {
    await audioPlayer.stop();
    update(['audioPlayer']);
  }
}
