import 'package:alquran_kita/apps/juz/juz.dart';

import '../sura/sura.dart';

enum ViewBy { sura, juz }

class VerseArgs {
  final Sura sura;
  final Juz juz;
  final int jumpTo;
  final ViewBy viewBy;

  VerseArgs({this.sura, this.juz, this.viewBy, this.jumpTo = 0});
}
