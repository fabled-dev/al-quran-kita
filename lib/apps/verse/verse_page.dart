import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:wakelock/wakelock.dart';

import '../admob/admob_controller.dart';
import '../audio/audio_player_view.dart';
import '../audio/download_audio_dialog.dart';
import '../home/jump_verse_dialog.dart';
import '../sura/sura_controller.dart';
import '../theme/app_theme_controller.dart';
import 'sura_card_view.dart';
import 'verse_args.dart';
import 'verse_controller.dart';

class VersePage extends StatelessWidget {
  final VerseArgs verseArgs = Get.arguments;
  final appTheme = ThemeController.to.appTheme;
  final verseController = VerseController.to;

  final ItemPositionsListener itemPositionsListener = ItemPositionsListener.create();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<VerseController>(
      initState: (_) {
        Wakelock.enable();
        verseController.setArgs(verseArgs);

        if (verseArgs.viewBy == ViewBy.sura) {
          verseController.appTitle = verseArgs.sura.latinName;
          verseController.getVerses(verseArgs.sura);
          int jumpTo = verseArgs.jumpTo.isNull ? 0 : verseArgs.jumpTo;
          if (jumpTo == 0) return;
          verseController.jumpTo = jumpTo;
          return;
        }

        if (verseArgs.viewBy == ViewBy.juz) {
          verseController.appTitle = verseArgs.juz.suraText;
          verseController.getVersesByJuz(verseArgs.juz);
        }

        Function changeTitleListener = () {
          final index = itemPositionsListener.itemPositions.value.first.index;
          final item = verseController.items[index];
          if (item is SuraCardView) {
            final title = item.sura.latinName;
            verseController.setTitle("Juz ${verseArgs.juz.number} / $title");
          }
        };

        itemPositionsListener.itemPositions.addListener(changeTitleListener);
      },
      dispose: (_) {
        Wakelock.disable();
        verseController.clear();
        AdmobController.to.showInterstitialAd();
      },
      builder: (_) {
        return Scaffold(
          appBar: _buildAppBar(),
          body: verseController.isBusy ? Container() : _buildVerseList(),
          floatingActionButton: AudioPlayerView(),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        );
      },
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      backgroundColor: Colors.white10,
      elevation: 0.0,
      leading: IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () => Get.back(),
        color: Colors.grey[600],
      ),
      title: GetBuilder<VerseController>(
        id: "title",
        builder: (_) {
          return Text(
            verseController.appTitle,
            style: TextStyle(color: appTheme.primaryColor, fontWeight: FontWeight.w600, fontSize: 16.0),
          );
        },
      ),
      actions: [
        IconButton(
          icon: Icon(Icons.low_priority),
          iconSize: 28.0,
          color: Colors.grey[700],
          onPressed: () async {
            int result = await Get.dialog(JumpSuraDialog(sura: verseController.args.sura));
            if (result == null) return;
            verseController.itemScrollController.jumpTo(index: result);
          },
        ),
        GetBuilder<SuraController>(
          id: "audioDownloaded",
          builder: (suraController) {
            if (verseController.args.viewBy == ViewBy.juz) return Container();
            if (verseController.args.sura.audioDownloaded) return Container();

            return IconButton(
              icon: Icon(Icons.file_download),
              iconSize: 28.0,
              color: Colors.grey[700],
              onPressed: () {
                Get.dialog(ConfirmDownloadAudioDialog(sura: verseController.args.sura));
              },
            );
          },
        ),
        SizedBox(width: 12.0),
      ],
    );
  }

  Widget _buildVerseList() {
    return NotificationListener<OverscrollIndicatorNotification>(
      onNotification: (overscroll) {
        overscroll.disallowGlow();
        return false;
      },
      child: ScrollablePositionedList.separated(
        itemCount: verseController.items.length,
        separatorBuilder: (context, index) => index == 0 ? Container() : Divider(indent: 22.0, endIndent: 22.0, height: 8.0),
        initialScrollIndex: verseController.jumpTo,
        itemScrollController: verseController.itemScrollController,
        itemPositionsListener: itemPositionsListener,
        itemBuilder: (context, index) {
          return verseController.items[index].build(context);
        },
      ),
    );
  }
}
