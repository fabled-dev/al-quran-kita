class Verse {
  final int id;
  final int sura;
  final int aya;
  final String text;
  final String nameTranslation;

  bool isLastRead;
  bool isBookmark;

  Verse({
    this.id,
    this.sura,
    this.aya,
    this.text,
    this.nameTranslation,
    this.isLastRead = false,
    this.isBookmark = false,
  });

  factory Verse.fromJson(Map json) {
    return Verse(
      id: json['id'],
      sura: json['sura'],
      aya: json['aya'],
      text: json['text'],
      nameTranslation: json['id_text'],
      isBookmark: json['number'] != null ? true : false,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "sura": sura,
      "aya": aya,
      "text": text,
      "id_text": nameTranslation,
    };
  }

  String get audioCode {
    return sura.toString().padLeft(3, '0') + aya.toString().padLeft(3, '0');
  }

  String get audioPath {
    return sura.toString() + '/' + aya.toString() + '.mp3';
  }
}
