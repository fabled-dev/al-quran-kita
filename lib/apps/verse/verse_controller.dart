import 'package:alquran_kita/apps/core/app_exception.dart';
import 'package:clipboard/clipboard.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

import '../audio/audio_controller.dart';
import '../audio/download_audio_dialog.dart';
import '../bookmark/bookmark.dart';
import '../bookmark/bookmark_controller.dart';
import '../home/home_controller.dart';
import '../home/last_read.dart';
import '../juz/juz.dart';
import '../sura/sura.dart';
import '../sura/sura_controller.dart';
import '../widgets/app_snackbar.dart';
import 'sura_card_view.dart';
import 'verse.dart';
import 'verse_args.dart';
import 'verse_item.dart';
import 'verse_repo.dart';
import 'verse_view.dart';

class VerseController extends GetxController {
  bool isBusy = true;

  final verseRepo = VerseRepo.to;
  final bookmarkController = BookmarkController.to;
  final homeController = HomeController.to;

  static VerseController get to => Get.find();
  static init() async {
    Get.put(VerseRepo());
    Get.put(VerseController());
  }

  VerseArgs args;
  void setArgs(VerseArgs value) => args = value;

  String appTitle = "";
  void setTitle(String value) {
    if (appTitle == value) return;
    appTitle = value;
    update(["title"]);
  }

  int jumpTo = 0;
  final ItemScrollController itemScrollController = ItemScrollController();
  void setJumpTo(int value) {
    jumpTo = value;
    itemScrollController.jumpTo(index: value);
  }

  List<Sura> suras = [];
  List<VerseItem> items = [];

  Future<void> getVerses(Sura sura) async {
    isBusy = true;
    sura.verses = await verseRepo.findVerseBySura(sura.number, limit: sura.versesCount);
    suras = [sura];
    setItems();
  }

  Future<void> getVersesByJuz(Juz juz) async {
    isBusy = true;
    suras = SuraController.to.suras.where((sura) => sura.number >= juz.startSura && sura.number <= juz.endSura).toList();
    int totalVerses = suras.fold(0, (sum, sura) => sum + sura.versesCount);
    int offset = juz.startVerse - 1;
    int limit = totalVerses - offset - suras.last.versesCount + juz.endVerse;
    List<int> suraIds = suras.map((sura) => sura.number).toList();

    final verses = await verseRepo.findVerseBySuras(suraIds, limit: limit, offset: offset);
    int start = 0;
    for (var sura in suras) {
      int end = start + sura.versesCount;
      if (sura.number == juz.startSura) end = start + sura.versesCount - juz.startVerse + 1;
      if (sura.number == juz.endSura) end = start + juz.endVerse;
      if (sura.number == juz.startSura && sura.number == juz.endSura) end = start + juz.endVerse - juz.startVerse + 1;
      sura.verses = verses.getRange(start, end).toList();
      start = end;
    }

    setItems();
  }

  void setItems() {
    setVerseLastRead();
    items.clear();
    for (var sura in suras) {
      items.add(SuraCardView(sura: sura));
      for (var verse in sura.verses) {
        items.add(VerseView(sura: sura, verse: verse));
      }
    }
    isBusy = false;
    update();
  }

  void setVerseLastRead() {
    if (homeController.lastRead.isNull) return;
    for (var sura in suras) {
      for (var verse in sura.verses) {
        verse.isLastRead = verse.id == homeController.lastRead.verse.id;
      }
    }
  }

  void copyVerse(Sura sura, Verse verse) {
    FlutterClipboard.copy(verse.text);
    AppSnackBar.simple("${sura.latinName}: Ayat ${verse.aya} berhasil di salin");
  }

  void saveLastRead(Sura sura, Verse verse) {
    verse.isLastRead = true;
    unbindOldVerse();
    homeController.saveLastRead(sura, verse);
    update(["${verse.id}"]);
    AppSnackBar.simple("Terakhir dibaca berhasil diubah ke \n ${sura.latinName}: Ayat ${verse.aya}");
  }

  void unbindOldVerse() {
    final box = GetStorage();
    Map<String, dynamic> boxLastRead = box.read('lastRead');
    if (boxLastRead.isNull) return;

    final lastRead = LastRead.fromJson(boxLastRead);
    final sura = suras.firstWhere((sura) => sura.number == lastRead.sura.number, orElse: () => null);
    if (sura.isNull) return;

    final verse = sura.verses.firstWhere((verse) => verse.id == lastRead.verse.id);
    verse.isLastRead = false;
    update(["${verse.id}"]);
  }

  void removeLastRead(Verse verse) {
    verse.isLastRead = false;
    homeController.removeLastRead();
    update(["${verse.id}"]);
  }

  Future<void> toogleBookmark(Sura sura, Verse verse) async {
    final isBookmark = verse.isBookmark;
    verse.isBookmark = !verse.isBookmark;
    update(["${verse.id}"]);
    if (isBookmark) return bookmarkController.deleteBookmarkByVerse(verse);

    final bookmark = Bookmark(number: 0, sura: sura, verse: verse);
    bookmarkController.addBookmark(bookmark);
    AppSnackBar.simple("${sura.latinName}: Ayat ${verse.aya} berhasil di bookmark");
  }

  void clear() {
    jumpTo = 0;
    suras.clear();
  }

  Future<void> playAudio(Sura sura, Verse verse) async {
    try {
      final audioController = AudioController.to;
      audioController.addPlayList(sura.verses);
      await audioController.startPlay(verse: verse);
    } on FileNotFoundException {
      Get.dialog(ConfirmDownloadAudioDialog(sura: sura));
    }
  }
}
