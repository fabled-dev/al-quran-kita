import 'package:flutter/material.dart';

abstract class VerseItem {
  Widget build(BuildContext context);
}
