import 'package:get/get.dart';
import 'package:sqflite/sqflite.dart';

import '../core/db_service.dart';
import 'verse.dart';

class VerseRepo extends GetxService {
  final table = "verses";
  List columns = ["id", "sura", "aya", "text", "id_text"];

  Database db = Get.find<DbService>().db;
  static VerseRepo get to => Get.find();

  Future<int> count() async {
    final res = await db.rawQuery("SELECT COUNT(*) FROM $table");
    return res[0]["COUNT(*)"] as int;
  }

  Future<List<Verse>> findVerseBySura(int suraId, {int limit = -1, int offset = -1}) async {
    final verses = await db.rawQuery("""
      SELECT verses.id, verses.sura, verses.aya, verses.text, verses.id_text, bookmarks.number FROM verses
      LEFT JOIN bookmarks ON verses.id = bookmarks.verse_id
      WHERE verses.sura = $suraId
      ORDER BY verses.id ASC
      LIMIT $limit OFFSET $offset;
    """);

    return verses.map((verse) => Verse.fromJson(verse)).toList();
  }

  Future<List<Verse>> findVerseBySuras(List<int> suraIds, {int limit = -1, int offset = -1}) async {
    final ids = suraIds.join(',');
    final verses = await db.rawQuery("""
      SELECT verses.id, verses.sura, verses.aya, verses.text, verses.id_text, bookmarks.number FROM verses
      LEFT JOIN bookmarks ON verses.id = bookmarks.verse_id
      WHERE verses.sura IN ($ids)
      ORDER BY verses.id ASC
      LIMIT $limit OFFSET $offset;
    """);
    return verses.map((verse) => Verse.fromJson(verse)).toList();
  }
}
