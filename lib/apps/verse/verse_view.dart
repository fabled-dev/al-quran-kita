import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../sura/sura.dart';
import '../theme/app_theme_controller.dart';
import 'verse.dart';
import 'verse_controller.dart';
import 'verse_item.dart';

class VerseView extends StatelessWidget implements VerseItem {
  final Verse verse;
  final Sura sura;
  final AppTheme appTheme = Get.find<ThemeController>().appTheme;

  VerseView({Key key, this.verse, this.sura}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(18.0, 16.0, 18.0, 8.0),
      child: Column(
        children: [
          Container(
            height: 50.0,
            padding: EdgeInsets.fromLTRB(12.0, 0.0, 4.0, 0.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12.0),
              color: Colors.grey[200].withAlpha(200),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  constraints: BoxConstraints(minWidth: 26, maxHeight: 26),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100.0),
                    border: Border.all(color: appTheme.primaryColor, width: 1.2),
                  ),
                  margin: EdgeInsets.symmetric(horizontal: 4.0),
                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                  alignment: Alignment.center,
                  child: Text("${verse.aya}", style: TextStyle(color: appTheme.primaryColor, fontWeight: FontWeight.w500)),
                ),
                Row(
                  children: [
                    _buildCopyIcon(),
                    _buildPlayAudioIcon(),
                    _buildLastReadIcon(),
                    _buildBookmarkIcon(),
                  ],
                )
              ],
            ),
          ),
          SizedBox(height: 16.0),
          Container(
            alignment: Alignment.topRight,
            padding: EdgeInsets.only(left: 40.0, right: 8.0),
            child: Text(
              '${verse.text}',
              style: TextStyle(fontFamily: "Arabic", fontSize: 22.0),
              textAlign: TextAlign.end,
            ),
          ),
          SizedBox(height: 16.0),
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(left: 8.0),
            child: Text(
              '${verse.nameTranslation}',
              style: TextStyle(fontSize: 12.0, height: 1.6),
              textAlign: TextAlign.left,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCopyIcon() {
    return IconButton(
      icon: Icon(Icons.content_copy),
      color: Colors.grey,
      iconSize: 18.0,
      onPressed: () => Get.find<VerseController>().copyVerse(sura, verse),
    );
  }

  Widget _buildPlayAudioIcon() {
    return IconButton(
      icon: Icon(Icons.play_arrow),
      color: Colors.grey,
      iconSize: 28.0,
      onPressed: () => VerseController.to.playAudio(sura, verse),
    );
  }

  Widget _buildLastReadIcon() {
    return GetBuilder<VerseController>(
      id: "${verse.id}",
      builder: (verseController) {
        if (verse.isLastRead) {
          return IconButton(
            icon: Icon(Icons.link),
            color: appTheme.activeColor,
            onPressed: () => verseController.removeLastRead(verse),
          );
        }

        return IconButton(
          icon: Icon(Icons.link),
          color: Colors.grey,
          onPressed: () => verseController.saveLastRead(sura, verse),
        );
      },
    );
  }

  Widget _buildBookmarkIcon() {
    return GetBuilder<VerseController>(
      id: "${verse.id}",
      builder: (verseController) {
        if (verse.isBookmark) {
          return IconButton(
            icon: Icon(Icons.bookmark),
            color: appTheme.activeColor,
            onPressed: () => verseController.toogleBookmark(sura, verse),
          );
        }

        return IconButton(
          icon: Icon(Icons.bookmark_border),
          color: Colors.grey,
          onPressed: () => verseController.toogleBookmark(sura, verse),
        );
      },
    );
  }
}
