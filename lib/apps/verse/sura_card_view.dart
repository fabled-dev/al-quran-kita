import 'package:alquran_kita/apps/theme/app_theme.dart';
import 'package:alquran_kita/apps/theme/app_theme_controller.dart';
import 'package:alquran_kita/apps/verse/verse_item.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../sura/sura.dart';

class SuraCardView extends StatelessWidget implements VerseItem {
  final Sura sura;
  final AppTheme appTheme = Get.find<ThemeController>().appTheme;

  SuraCardView({Key key, this.sura}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 12.0,
      shadowColor: appTheme.primaryColor,
      margin: EdgeInsets.fromLTRB(22.0, 16.0, 22.0, 16.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
      child: Container(
        height: 175,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16.0),
          gradient: RadialGradient(
            center: Alignment(-1, -1),
            radius: 2.8,
            colors: [appTheme.gradientColorPrimary, appTheme.gradientColorSecondary],
            tileMode: TileMode.clamp,
          ),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(12.0),
          child: Stack(
            children: [
              Positioned(
                right: 0.0,
                bottom: -90.0,
                child: Container(
                  width: 280.0,
                  child: Opacity(opacity: 0.12, child: Image.asset('assets/quran.png')),
                ),
              ),
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(left: 32, right: 32.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: 24),
                    Text(sura.translationName, style: TextStyle(fontSize: 16.0, color: Colors.white, fontWeight: FontWeight.w500)),
                    SizedBox(height: 12),
                    Divider(color: Colors.white, thickness: 1.6, indent: 16.0, endIndent: 16.0),
                    SizedBox(height: 12),
                    Text(
                      "${sura.place} - ${sura.versesCount} Ayat".toUpperCase(),
                      style: TextStyle(fontSize: 15.0, color: Colors.white, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(height: 12),
                    Text(
                      "${sura.arabicName}",
                      style: TextStyle(fontFamily: "Arabic", fontSize: 32.0, color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 8),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
