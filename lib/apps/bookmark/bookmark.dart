import '../sura/sura.dart';
import '../verse/verse.dart';

class Bookmark {
  final int number;
  final Sura sura;
  final Verse verse;
  final DateTime createdAt;

  Bookmark({this.number, this.sura, this.verse, this.createdAt});

  factory Bookmark.fromJson(json) {
    return Bookmark(
      number: json['number'],
      createdAt: DateTime.parse(json['created_at']),
      verse: Verse.fromJson(json['verse']),
      sura: Sura.fromJson(json['sura']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'number': number,
      'verse': verse.toJson(),
      'sura': sura.toJson(),
    };
  }

  Map<String, dynamic> toDatabase() {
    return {
      'number': number,
      'verse_id': verse.id,
    };
  }
}
