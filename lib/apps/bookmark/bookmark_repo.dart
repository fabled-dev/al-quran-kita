import 'package:get/get.dart';
import 'package:sqflite/sqflite.dart';

import '../core/db_service.dart';
import 'bookmark.dart';

class BookmarkRepo extends GetxService {
  Database db = Get.find<DbService>().db;
  static BookmarkRepo get to => Get.find();

  final table = "bookmarks";

  Future<int> count() async {
    final res = await db.rawQuery("SELECT COUNT(*) FROM $table");
    return res[0]["COUNT(*)"] as int;
  }

  Future<List<Bookmark>> find() async {
    final bookmarks = await db.rawQuery("""
      SELECT  bookmarks.number, bookmarks.created_at,
              verses.id AS verse_id, verses.sura AS verse_sura, verses.aya AS verse_aya,
              suras.number AS sura_number, suras.place AS sura_place, suras."order" AS sura_order, suras.arabic_name AS sura_arabic_name, suras.latin_name AS sura_latin_name, suras.verses_count AS sura_verses_count, suras.id_name AS sura_id_name
      FROM bookmarks
      LEFT JOIN verses ON bookmarks.verse_id = verses.id
      LEFT JOIN suras ON verses.sura = suras.number
      ORDER BY verses.id ASC
    """);

    return bookmarks.map((bookmark) {
      Map mapBookmark = {
        'number': bookmark['number'],
        'created_at': bookmark['created_at'],
        'verse': {
          'id': bookmark['verse_id'],
          'aya': bookmark['verse_aya'],
          'sura': bookmark['verse_sura'],
        },
        'sura': {
          'number': bookmark['sura_number'],
          'order': bookmark['sura_order'],
          'place': bookmark['sura_place'],
          'arabic_name': bookmark['sura_arabic_name'],
          'latin_name': bookmark['sura_latin_name'],
          'verses_count': bookmark['sura_verses_count'],
          'id_name': bookmark['sura_id_name'],
        }
      };
      return Bookmark.fromJson(mapBookmark);
    }).toList();
  }

  Future<void> create(Bookmark bookmark) async {
    await db.insert(
      table,
      bookmark.toDatabase(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> delete(int verseId) async {
    await db.delete(table, where: 'verse_id = ?', whereArgs: [verseId]);
  }
}
