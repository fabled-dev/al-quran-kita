import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../home/app_tab_view.dart';
import '../home/last_read_view.dart';
import 'bookmark_controller.dart';
import 'bookmark_view.dart';

class BookmarkPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<BookmarkController>(
      initState: (state) {
        BookmarkController.to.getBookmarks();
      },
      builder: (bookmarkController) {
        return ListView.builder(
          physics: BouncingScrollPhysics(),
          shrinkWrap: true,
          padding: EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 16.0),
          itemCount: 2,
          itemBuilder: (context, index) {
            if (index == 0) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [LastReadView(), AppTabView()],
              );
            }

            int count = ((MediaQuery.of(context).size.width - 48) / 120).floor();

            return GridView.builder(
              shrinkWrap: true,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: count,
                childAspectRatio: 1.8,
                mainAxisSpacing: 8.0,
                crossAxisSpacing: 8.0,
              ),
              physics: NeverScrollableScrollPhysics(),
              padding: EdgeInsets.fromLTRB(16.0, 4.0, 16.0, 16.0),
              itemCount: bookmarkController.bookmarks.length,
              itemBuilder: (context, index) {
                final bookmark = bookmarkController.bookmarks[index];
                return BookmarkView(bookmark: bookmark);
              },
            );
          },
        );
      },
    );
  }
}
