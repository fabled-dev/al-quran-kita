import 'package:alquran_kita/apps/theme/app_theme_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../verse/verse_args.dart';
import 'bookmark.dart';

class BookmarkView extends StatelessWidget {
  final Bookmark bookmark;
  final appTheme = ThemeController.to.appTheme;

  BookmarkView({Key key, this.bookmark}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        final args = VerseArgs(jumpTo: bookmark.verse.aya, sura: bookmark.sura, viewBy: ViewBy.sura);
        return Get.toNamed("/verse", arguments: args);
      },
      child: Container(
        padding: EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
          border: Border.all(color: appTheme.gradientColorSecondary, width: 1.2),
          color: appTheme.gradientColorSecondary.withAlpha(16),
        ),
        child: Opacity(
          opacity: 0.85,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "${bookmark.sura.latinName}",
                textAlign: TextAlign.center,
                maxLines: 2,
                style: TextStyle(fontSize: 13.0, fontWeight: FontWeight.w500, color: appTheme.darkPrimaryColor),
              ),
              SizedBox(height: 4.0),
              Text(
                "${bookmark.sura.number} : ${bookmark.verse.aya}",
                style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w500, color: appTheme.darkPrimaryColor),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
