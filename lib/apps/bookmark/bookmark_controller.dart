import 'package:get/get.dart';

import '../verse/verse.dart';
import 'bookmark.dart';
import 'bookmark_repo.dart';

class BookmarkController extends GetxController {
  List<Bookmark> bookmarks = [];
  bool isInit = false;

  static BookmarkController get to => Get.find();
  final bookmarkRepo = Get.find<BookmarkRepo>();

  static init() {
    Get.put(BookmarkRepo());
    Get.put(BookmarkController());
  }

  Future<void> getBookmarks() async {
    if (isInit) return;
    bookmarks = await bookmarkRepo.find();
    isInit = true;
    update();
  }

  void addBookmark(Bookmark bookmark) {
    bookmarkRepo.create(bookmark);
    bookmarks.add(bookmark);
    bookmarks.sort((a, b) => a.verse.id.compareTo(b.verse.id));
    update();
  }

  void deleteBookmark(Bookmark bookmark) {
    bookmarkRepo.delete(bookmark.verse.id);
    bookmarks.remove(bookmark);
    update();
  }

  void deleteBookmarkByVerse(Verse verse) {
    bookmarkRepo.delete(verse.id);
    bookmarks.removeWhere((bookmark) => bookmark.verse.id == verse.id);
    update();
  }
}
