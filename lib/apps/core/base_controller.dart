import 'package:get/get.dart';

class BaseController extends GetxController {
  bool isBusy = false;
  setBusy(bool value) {
    if (isBusy == value) return;
    isBusy = value;
    update();
  }

  String errorMessage;
  Future<void> run(Future<void> Function() body, {Future<void> Function(dynamic error) onError}) async {
    try {
      setBusy(true);
      errorMessage = null;
      await body();
    } catch (e) {
      errorMessage = 'error';
      if (onError != null) await onError(e);
    } finally {
      setBusy(false);
    }
  }
}
