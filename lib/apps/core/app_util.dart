class AppUtil {
  static List<List<T>> chunk<T>(List<T> items, int size) {
    int length = items.length;
    List<List<T>> chunks = [];

    for (var i = 0; i < length; i += size) {
      var end = (i + size < length) ? i + size : length;
      chunks.add(items.sublist(i, end));
    }

    return chunks;
  }
}
