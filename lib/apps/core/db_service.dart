import 'dart:async';
import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DbService extends GetxService {
  Database db;

  static init() async {
    final dbService = DbService();
    await dbService.initDb();
    Get.put(dbService);
  }

  Future<void> initDb() async {
    final path = join(await getDatabasesPath(), "quran.db");
    // await deleteDatabase(path);
    db = await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
      onUpgrade: _onUpgrade,
    );
  }

  FutureOr<void> _onCreate(Database db, int version) async {
    await db.execute(createTableSura());
    await seedSuras(db);
    await db.execute(createTableVerse());
    await seedVerses(db);
    await db.execute(createTableJuz());
    await seedJuzs(db);

    await db.execute(createTableBookmarkVerse());
  }

  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    // if (newVersion == 2) {
    //   await db.execute(createTableJuz());
    //   await seedJuzs(db);
    //   return;
    // }
  }

  String createTableSura() {
    return """
      CREATE TABLE suras (
        number INTEGER PRIMARY KEY,
        place TEXT NOT NULL,
        "order" INTEGER NOT NULL,
        arabic_name TEXT NOT NULL,
        latin_name TEXT NOT NULL,
        verses_count INTEGER NOT NULL,
        id_name TEXT NOT NULL
      );
      CREATE INDEX number_idx ON suras (number);
      """;
  }

  String createTableVerse() {
    return """
      CREATE TABLE verses (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        sura INTEGER NOT NULL,
        aya INTEGER NOT NULL,
        text TEXT NOT NULL,
        id_text TEXT NOT NULL,
        FOREIGN KEY(sura) REFERENCES suras(number)
      );
      CREATE INDEX sura_idx ON verses (sura);
      CREATE INDEX aya_idx ON verses (aya);
      """;
  }

  String createTableJuz() {
    return """
      CREATE TABLE juzs (
        number INTEGER PRIMARY KEY,
        start_sura INTEGER,
        end_sura INTEGER,
        start_verse INTEGER,
        end_verse INTEGER,
        FOREIGN KEY(start_sura) REFERENCES suras(number)
      );
      CREATE INDEX number_idx ON suras (number);
      """;
  }

  String createTableBookmarkVerse() {
    return """
      CREATE TABLE bookmarks (
        number INTEGER NOT NULL,
        verse_id INTEGER NOT NULL,
        created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
        FOREIGN KEY(verse_id) REFERENCES verses(id)
      );
      CREATE INDEX number_idx ON bookmark_verses (number);
      """;
  }

  Future<void> seedSuras(db) async {
    print("Start Seeding Suras...");
    final surasFile = await rootBundle.loadString("assets/quran-data/suras.id.json");
    Iterable surasJson = jsonDecode(surasFile)['chapters'];
    final table = "suras";
    final batch = db.batch();

    for (var suraJson in surasJson) {
      batch.insert(table, {
        "number": suraJson['chapter_number'],
        "order": suraJson['revelation_order'],
        "place": suraJson['revelation_place'],
        "arabic_name": suraJson['name_arabic'],
        "latin_name": suraJson['name_simple'],
        "verses_count": suraJson['verses_count'],
        "id_name": suraJson['translated_name']['name'],
      });
    }

    await batch.commit(noResult: true);
    print("Seeding Suras Completed...");
  }

  Future<void> seedVerses(db) async {
    print("Start Seeding Verses...");
    final quranArabicString = await rootBundle.loadString("assets/quran-data/quran_ar.txt");
    final quranIndoString = await rootBundle.loadString("assets/quran-data/quran_id.txt");
    final versesArab = LineSplitter.split(quranArabicString).toList();
    final versesIndo = LineSplitter.split(quranIndoString).toList();
    final table = "verses";
    final batch = db.batch();

    for (var index = 0; index < versesArab.length; index++) {
      final verseArab = versesArab[index].split("|");
      final verseIndo = versesIndo[index].split("|");
      batch.insert(table, {
        "sura": verseArab[0],
        "aya": verseArab[1],
        "text": verseArab[2],
        "id_text": verseIndo[2],
      });
    }

    await batch.commit(noResult: true);
    print("Seeding Verses Completed...");
  }

  Future<void> seedJuzs(db) async {
    print("Start Seeding Juzs...");
    final juszFile = await rootBundle.loadString("assets/quran-data/juzs.json");
    Iterable juzsJson = jsonDecode(juszFile);
    final table = "juzs";
    final batch = db.batch();

    for (var juzJson in juzsJson) {
      batch.insert(table, {
        "number": juzJson['chapter_number'],
        "start_sura": juzJson['start']['index'],
        "end_sura": juzJson['end']['index'],
        "start_verse": juzJson['start']['verse'],
        "end_verse": juzJson['end']['verse'],
      });
    }

    await batch.commit(noResult: true);
    print("Seeding Juzs Completed...");
  }
}
