import 'package:alquran_kita/apps/sura/sura_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../widgets/app_icon_icons.dart';
import '../verse/verse_args.dart';
import '../theme/app_theme_controller.dart';
import 'sura.dart';

class SuraView extends StatelessWidget {
  final Sura sura;
  final AppTheme appTheme = Get.find<ThemeController>().appTheme;

  SuraView({Key key, @required this.sura}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        final args = VerseArgs(sura: sura, viewBy: ViewBy.sura);
        Get.toNamed("/verse", arguments: args);
        if (sura.statusDownload != StatusDownload.unknown) return;
        SuraController.to.createAudioDirectory(sura: sura);
      },
      child: Container(
        padding: EdgeInsets.fromLTRB(4.0, 4.0, 4.0, 4.0),
        child: Row(
          children: [
            buildSuraNumber(),
            buildSuraText(),
            buildSuraArabicName(),
            SizedBox(width: 8.0),
          ],
        ),
      ),
    );
  }

  Widget buildSuraNumber() {
    return Stack(
      alignment: Alignment.center,
      children: [
        Text("${sura.number}", style: TextStyle(fontSize: 13, color: appTheme.primaryColor)),
        Icon(AppIcon.star, size: 46.0, color: appTheme.primaryColor),
      ],
    );
  }

  Widget buildSuraText() {
    return Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.only(left: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text("${sura.latinName}", style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500, color: appTheme.darkPrimaryColor)),
              SizedBox(width: 4.0),
              Text("(${sura.versesCount})", style: TextStyle(fontSize: 12.0, color: Colors.grey)),
            ],
          ),
          SizedBox(height: 8.0),
          Text("${sura.translationName}", style: TextStyle(fontSize: 12.5, color: Colors.grey)),
        ],
      ),
    );
  }

  Widget buildSuraArabicName() {
    return Expanded(
      child: Text(
        sura.arabicName,
        style: TextStyle(fontSize: 24.0, color: appTheme.primaryColor, fontFamily: "Arabic"),
        textAlign: TextAlign.right,
      ),
    );
  }
}
