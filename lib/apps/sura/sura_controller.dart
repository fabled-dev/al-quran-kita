import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../core/app_exception.dart';
import '../verse/verse.dart';
import '../widgets/app_snackbar.dart';
import 'sura.dart';
import 'sura_repo.dart';

class SuraController extends GetxController {
  List<Sura> suras = [];

  static SuraController get to => Get.find();
  final suraRepo = Get.find<SuraRepo>();

  static Directory dir;
  static init() async {
    Get.put(SuraRepo());
    final suraController = SuraController();
    dir = await getApplicationDocumentsDirectory();
    await suraController.getSuras();
    Get.put(suraController);
  }

  Future<void> getSuras({lang = 'id'}) async {
    if (suras.length != 0) return;
    suras = await suraRepo.find();
    update();
  }

  Future<void> downloadAllAudio(Sura sura) async {
    String reciteBy = 'afasy';
    String path = "${dir.path}/audio/$reciteBy/${sura.number}/";
    sura.statusDownload = StatusDownload.progress;
    sura.totalAudio = 0;
    update(['totalAudio']);

    Dio dio = Dio();
    int index = -1;
    int maxWorker = min(16, sura.verses.length - 1);

    Future createWorker() async {
      index += 1;
      Verse verse = sura.verses[index];
      String url = "http://tanzil.net/res/audio/$reciteBy/${verse.audioCode}.mp3";
      await dio.download(url, path + '${verse.aya}.mp3');
      sura.totalAudio += 1;
      update(['totalAudio']);
      if (sura.statusDownload == StatusDownload.cancel) throw CancelDownloadException();
      if (index < sura.verses.length - 1) return createWorker();
    }

    try {
      List<Future> commands = List.generate(maxWorker, (i) => createWorker());
      await Future.wait(commands);
      update(['audioDownloaded']);
      sura.statusDownload = StatusDownload.finish;
      Get.back();
    } on DioError {
      Get.back();
      sura.statusDownload = StatusDownload.unfinished;
      AppSnackBar.simple("Download audio gagal, silakan cek kembali koneksi internet anda");
    } on CancelDownloadException {
      sura.statusDownload = StatusDownload.unfinished;
    } catch (e) {
      sura.statusDownload = StatusDownload.unstarted;
      Get.back();
    }
  }

  cancelDownload({Sura sura}) {
    sura.statusDownload = StatusDownload.cancel;
  }

  Future<String> createAudioDirectory({Sura sura}) async {
    String reciteBy = 'afasy';
    String path = "${dir.path}/audio/$reciteBy/${sura.number}/";
    Directory audioDir = Directory(path);
    bool isExist = await audioDir.exists();

    if (!isExist) {
      sura.statusDownload = StatusDownload.unstarted;
      await audioDir.create(recursive: true);
      return audioDir.path;
    }

    List<FileSystemEntity> files = audioDir.listSync();
    sura.totalAudio = files.length;
    update(['audioDownloaded']);
    if (sura.audioDownloaded) {
      sura.statusDownload = StatusDownload.unstarted;
      return audioDir.path;
    }

    sura.statusDownload = StatusDownload.unfinished;
    return audioDir.path;
  }
}
