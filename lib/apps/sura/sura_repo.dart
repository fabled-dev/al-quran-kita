import 'package:alquran_kita/apps/verse/verse.dart';
import 'package:get/get.dart';
import 'package:sqflite/sqflite.dart';

import '../core/db_service.dart';
import 'sura.dart';

class SuraRepo extends GetxService {
  final table = "suras";

  Database db = Get.find<DbService>().db;
  static SuraRepo get to => Get.find();

  Future<List<Sura>> find() async {
    final suras = await db.query(table);
    return suras.map((verse) => Sura.fromJson(verse)).toList();
  }

  Future<List<Sura>> findWithVerses({word}) async {
    final items = await db.rawQuery("""
      SELECT
          suras.*,
          verses.id, verses.sura as verse_sura, verses.aya, verses.text, verses.id_text FROM suras
      LEFT JOIN verses ON verses.sura = suras.number
      WHERE verses.id_text LIKE '%$word%'
      ORDER BY suras.number ASC, verses.id ASC
    """);

    List<Sura> suras = [];
    if (items.length == 0) return suras;

    int indexSura = 0;
    suras.add(Sura.fromJson(items[0]));

    for (var item in items) {
      if (suras[indexSura].number != item['number']) {
        final sura = Sura.fromJson(item);
        suras.add(sura);
        indexSura += 1;
      }

      final verse = Verse.fromJson(item);
      suras[indexSura].verses.add(verse);
    }

    return suras;
  }
}
