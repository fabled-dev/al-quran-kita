import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../home/app_tab_view.dart';
import '../home/last_read_view.dart';
import 'sura_controller.dart';
import 'sura_view.dart';

class SuraPage extends StatelessWidget {
  final SuraController suraController = SuraController.to;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SuraController>(
      initState: (state) => suraController.getSuras(),
      builder: (suraController) {
        return ListView.separated(
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 16.0),
          separatorBuilder: (context, index) => index == 0 ? Container() : Divider(indent: 16.0, endIndent: 16.0),
          itemCount: suraController.suras.length + 1,
          itemBuilder: _buildItemBuilder,
        );
      },
    );
  }

  Widget _buildItemBuilder(BuildContext context, int index) {
    if (index == 0) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [LastReadView(), AppTabView()],
      );
    }

    index--;
    final sura = suraController.suras[index];
    return SuraView(sura: sura);
  }
}
