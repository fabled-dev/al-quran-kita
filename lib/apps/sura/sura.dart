import '../verse/verse.dart';

enum StatusDownload { unknown, progress, unstarted, unfinished, finish, cancel }

class Sura {
  final int number;
  final int order;
  final String place;
  final String arabicName;
  final String latinName;
  final int versesCount;
  final String translationName;

  List<Verse> verses = [];
  int totalAudio = 0;
  StatusDownload statusDownload = StatusDownload.unknown;

  Sura({this.number, this.order, this.place, this.arabicName, this.latinName, this.versesCount, this.translationName});

  factory Sura.fromJson(Map json) {
    return Sura(
      number: json['number'],
      order: json['order'],
      place: json['place'],
      arabicName: json['arabic_name'],
      latinName: json['latin_name'],
      versesCount: json['verses_count'],
      translationName: json['id_name'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "number": number,
      "order": order,
      "place": place,
      "arabic_name": arabicName,
      "latin_name": latinName,
      "verses_count": versesCount,
      "id_name": translationName,
    };
  }

  bool get audioDownloaded {
    return totalAudio == versesCount;
  }

  double get percentDownloaded {
    return (totalAudio / versesCount * 100);
  }
}
