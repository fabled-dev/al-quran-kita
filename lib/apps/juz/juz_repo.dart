import 'package:alquran_kita/apps/juz/juz.dart';
import 'package:get/get.dart';
import 'package:sqflite/sqflite.dart';

import '../core/db_service.dart';
import 'juz.dart';

class JuzRepo extends GetxService {
  final table = "juzs";

  Database db = Get.find<DbService>().db;
  static JuzRepo get to => Get.find();

  Future<List<Juz>> find() async {
    final juzs = await db.rawQuery("""
      SELECT juzs.number, start_sura, end_sura, start_verse, end_verse, suras.latin_name
      FROM $table
      LEFT JOIN suras ON $table.start_sura = suras.number
    """);
    return juzs.map((verse) => Juz.fromJson(verse)).toList();
  }
}
