import 'package:alquran_kita/apps/juz/juz_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../home/app_tab_view.dart';
import '../home/last_read_view.dart';
import 'juz_controller.dart';

class JuzPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<JuzController>(
      initState: (state) {
        JuzController.to.getJuzs();
      },
      builder: (juzController) {
        return ListView.separated(
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 16.0),
          separatorBuilder: (context, index) => index == 0 ? Container() : Divider(indent: 16.0, endIndent: 16.0),
          itemCount: juzController.juzs.length + 1,
          itemBuilder: (context, index) {
            if (index == 0) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [LastReadView(), AppTabView()],
              );
            }

            index--;
            final juz = juzController.juzs[index];
            return JuzView(juz: juz);
          },
        );
      },
    );
  }
}
