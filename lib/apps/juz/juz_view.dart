import 'package:alquran_kita/apps/verse/verse_args.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../theme/app_theme_controller.dart';
import 'juz.dart';

class JuzView extends StatelessWidget {
  final Juz juz;
  final appTheme = ThemeController.to.appTheme;

  JuzView({Key key, @required this.juz}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        final args = VerseArgs(juz: juz, viewBy: ViewBy.juz);
        Get.toNamed("/verse", arguments: args);
      },
      child: Container(
        padding: EdgeInsets.fromLTRB(18.0, 4.0, 4.0, 4.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Juz ${juz.number}", style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500, color: appTheme.darkPrimaryColor)),
            SizedBox(height: 8.0),
            Text("${juz.suraText} QS. ${juz.startSura} : ${juz.startVerse}", style: TextStyle(fontSize: 14.0, color: Colors.grey)),
          ],
        ),
      ),
    );
  }
}
