import 'package:alquran_kita/apps/sura/sura.dart';

class Juz {
  final int number;
  final int startSura;
  final int endSura;
  final int startVerse;
  final int endVerse;
  final String suraText;
  List<Sura> suras;

  Juz({this.number, this.startSura, this.endSura, this.startVerse, this.endVerse, this.suraText});

  factory Juz.fromJson(Map json) {
    return Juz(
      number: json['number'],
      startSura: json['start_sura'],
      endSura: json['end_sura'],
      startVerse: json['start_verse'],
      endVerse: json['end_verse'],
      suraText: json['latin_name'],
    );
  }

  @override
  String toString() {
    return "number: $number, startSura: $startSura, endSura: $endSura, startVerse: $startVerse, endVerse: $endVerse";
  }
}
