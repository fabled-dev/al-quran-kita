import 'package:get/get.dart';

import 'juz.dart';
import 'juz_repo.dart';

class JuzController extends GetxController {
  List<Juz> juzs = [];

  static JuzController get to => Get.find();
  final juzRepo = Get.find<JuzRepo>();

  static init() async {
    Get.put(JuzRepo());
    Get.put(JuzController());
  }

  Future<void> getJuzs() async {
    if (juzs.length != 0) return;
    juzs = await juzRepo.find();
    update();
  }
}
