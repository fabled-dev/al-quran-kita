import 'package:get_storage/get_storage.dart';

import 'apps/admob/admob_controller.dart';
import 'apps/audio/audio_controller.dart';
import 'apps/bookmark/bookmark_controller.dart';
import 'apps/core/db_service.dart';
import 'apps/home/home_controller.dart';
import 'apps/juz/juz_controller.dart';
import 'apps/sura/sura_controller.dart';
import 'apps/verse/verse_controller.dart';

Future<void> setup() async {
  await GetStorage.init();
  await DbService.init();
  await AdmobController.init();
  await HomeController.init();
  await BookmarkController.init();
  await SuraController.init();
  await VerseController.init();
  await JuzController.init();
  await AudioController.init();
}
