import 'package:get/get.dart';

import 'apps/home/home_page.dart';
import 'apps/verse/verse_page.dart';

List<GetPage> routes = [
  GetPage(name: "/", page: () => HomePage()),
  GetPage(name: "/verse", page: () => VersePage(), transition: Transition.cupertino),
];
