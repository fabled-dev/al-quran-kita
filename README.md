# Alquran Kita

## Features

- [x] Daftar Surah
- [x] Daftar Juz
- [x] Download audio murottal dari Misyari Rasyid Al-'Afasi
- [x] Audio control murottal (play, pause, stop, next, prev)
- [x] Pencarian ayat-ayat berdasarkan terjemahan
- [x] Dapat loncat ke Surah dan ayat apapun
- [x] Bookmark ayat-ayat yang kamu sukai
- [x] Penanda bacaan terakhir
- [x] Copy ayat-ayat Alquran

## Todo List

- [ ] Berbagai macam tema termasuk mode terang dan gelap
- [ ] Pilihan penejermah
- [ ] Multi Language App (Indonesia, Inggris, dkk)
- [ ] Pilihan gaya penulisan Quran (IndoPak, Uthmani)
- [ ] Pilihan audio murottal
- [ ] Geser Layar Surah berikutnya dan sebaliknya

## Future

- [ ] Share ayat
- [ ] Tafsir
- [ ] Tulisan latin & warna pada tulisan quran
- [ ] Pengingat jadwal Sholat & Adzan
- [ ] Server / Firebase untuk simpan audio, sync bookmark, dkk

## Links

### Resources

- [Tanzil](http://tanzil.net/)

### Google

- [Google Drive](https://drive.google.com/drive/folders/12Dk5YQvLyo8abwlqkpexVQu5Z4tIeiJQ?usp=sharing)
- [Google Play](https://play.google.com/store/apps/details?id=com.fabled.alqurankita)
