# Changelog

## v1.1.0

- Implementasi admob
- Pencarian ayat-ayat berdasarkan terjemahan
- Download audio murottal dari Misyari Rasyid Al-'Afasi
- Audio control murottal (play, pause, stop, next, prev)
